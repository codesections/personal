+++
title = "emsenn"
description = "developer & writer"
+++

{% admonition(title="About me") %}
My name is emsenn. I'm an independent consultant and writer who specializes in teaching senior executives how they can improve their business by adapting methods used in open-source software development. 
{% end %}
