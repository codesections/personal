+++
title = "Personal Log"
description = "A log of things I've done I felt worth recording."
+++

### 2018-07-21
- Refactored [Brutstrap](https://emsenn.gitlab.io/brutstrap/), but didn't get into a working commit.

### 2018-07-20
- Played Planetside 2 with my friend Tom.
- Read about web accessibility.
<!-- more -->
### 2018-07-18
- Met my partner's sister and her family.

### 2018-07-16
- Released [Brutstrap for the Gutenberg static site generator](https://gitlab.com/emsenn/brutstrap-gutenberg/) version 0.1.0.
- Created the [Homepage To-Do](/homepage-todo.html).
- Wrote the guide for keeping a [PhOSS To-Do](/docs/phoss/todo/).

### 2018-07-15
- Released [Brutstrap](https://emsenn.gitlab.io/brutstrap/) version 1.0.0.

### 2018-07-14
- Released [my homepage](https://emsenn.net) version 0.2.0 through 1.0.3.

{{ subscriptionform(cta="Keep updated with all my work: subscribe to my mailing list.") }}

### 2018-07-12
- Released [personal CSS](https://emsenn.gitlab.io/csss/) version 0.1.0
- Released [my homepage](https://emsenn.gitlab.io) version 0.1.0.
- Released [Brutstrap](https://emsenn.gitlab.io/brutstrap/) version 0.3.2.

### 2018-07-11
- Released [Brutstrap](https://emsenn.gitlab.io/brutstrap/) versions 0.3.0 and 0.3.1.

### 2018-07-10
- Released [Brutstrap](https://emsenn.gitlab.io/brutstrap/) versions 0.1.0 to 0.2.1.

{% admonition(title="Notice") %}
Beyond this point, my log is reconstructed from information I could determine through other sources.
{% end %}

### 2018-05-10
- **Read** [an article about the effectiveness of targeted advertising](https://theintercept.com/2018/05/09/facebook-ads-tracking-algorithm/).

### 2018-04-26
- **Read** Andrew Roach's [definition of a "user-friendly computer"](http://ajroach42.com/observations-on-modern-computing-the-last-10-years-were-a-misstep/).

### 2018-04-12
- **Set up** an OpenBSD web & email server.

### 2018-03-30
- **Received** a portrait from my little sister.
{% figure(img="/assets/portrait-by-chloe-2018-03-30.jpg", alt="A digital sketch of a person with large eyebrows saying 'super-complicated big words', captioned 'An accurate depiction of my brother.'") %}
I'm... flattered.
{% end %}

### 2018-03-27
- **Visited** my family in Ohio.

### 2018-03-14
- **Read** Sarah Aswell's piece ["How Facebook Is Killing Comedy"](http://www.vulture.com/2018/02/how-facebook-is-killing-comedy.html).

### 2017-12-15
- **Prepared** an amazing and aesthetic mug of hot chocolate.
{% figure(img="/assets/hot-chocolate.jpg", alt="A large blue mug overflowing with whipped cream topped with shaved chocolate.") %}
Well worth the mess.
{% end %}

### 2016-08-06
- **Popularized** the belief that Donald Trump had a brain tumour revealed by a colonoscopy.

### 2016-07-20
- **Popularized** the minimalist _Don't Tread On Me_ flag.
{% figure(img="/assets/minimalist-dont-tread-on-me.jpg", alt="A yellow rectangle on a white background, with two black lines in it.") %}
The most minimal of the minimalist Don't Tread On Me flags I've done. Yes, it's meant to have that massive white border and be blurry.
{% end %}

### 2016-04-02
- **Began to spread a rumor** that Donald Trump is a time traveler.

### 2015-11-22
- **Began to popularize** the Linda Glocke "I will destroy ISIS" meme.

### 2015-10-03
- **Began to promote** the "Ted Cruz is the Zodiac Killer" memme.

### 2015-05-07
- **Had coffee** with my partner for the first time.

### 2015-03-??
- **Walked** with a cane for the first time. _(As of 2018-07-21, I still do.)_

### 2015-12-??
- **Walked** with crutches for the first time - I mean, since the accident on July 2nd.

### 2014-12-03
- **Celebrated** the passage of legislation I helped write, establishing that Iranians don't need to disclose their motive for requesting public information from the government.

### 2014-10-13
- **Resigned** from the Board of the Three Corners Collective after being unable to resolve a disagreement.

### 2014-07-18
- **Was released** from the hospital after the accident that occured on July 2nd. Moved in with local friends.
- **Renamed** the Sam Jones House, as I was unable to continue living there, to Sam Jones Media, and tried to refocus on helping local artists bring their work to production.

### 2014-07-02
- **Got hit** by a minivan that crossed into my lane. Broke a significant portion of my... everything.

### 2014-04-22
- **Founded**, along with others, the Three Corners Collective, a company focused on raising the average income of artists in the area.

### 2014-04-10
- **Founded** the Sam Jones House, a community space operated out of my residence.

### 2012-07-03
- **Began working** for a non-profit political organization for a few months.

### 2012-03-29
- **Moved** to Chapel Hill, North Carolina.

### 2012-01-02
- **Moved** to New York City.

### 2011-09-21
- **Moved** to Washington, D.C.

### 2009-07-12
- **Founded** Sedazad (then NedaNet) in response to the Iranian presidential election.

### 2008-??-??
- **Earned** my high-school diploma.

### 2003-07-21
- **Started attending** Stivers School for the Arts, studying creative writing with a focus on journalism.

### 2000-03-??
- **Moved** to Dayton, Ohio.

### 1999-??-??
- **Participated** in a collaboration between his elementary school and the NASA branch of the local Air Force Base.

### 1994-??-??
- **Immigrated** to the Kettering, Ohio, United States.

### 1990-05-11
- **Born** in Switzerland.

