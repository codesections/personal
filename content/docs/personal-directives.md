+++
title = "Personal Directives"
description = "My rules to live by."
+++
# Personal Directives

## 001
> Aggression is unethical.

Aggression is defined as an action which affects another agent without their consent.

---

## 002
> Data should be private.

Data carriers should not be able to know what they're carrying.

---

## 003
> Useful information cannot be owned.

Information ownership & prohibition cost more than they are worth.

---

## 004
> Competition should be purposeful.

Unless a competition has a clear goal, is guaranteed to create innovation greater than its cost, or is fun, it should not be engaged in.

---

## 005
> Figurative language is lazy.

Things can be beautifully described as they are. This extends to 
software: "an ounce of application is worth a ton of abstraction."

---

## 006
> Data should be parseable by humans.

The cost to computers is minimal compared to the benefit for humans.

---

## 007
> Depricating deviation from defaults should be motivated.

Changes to a system which make it incompatible with its default operating 
mode should only be done if there's a good reason. This is of extra 
importance to those who are tutors, as they will often be interacting 
with default systems & their users.

---

## 008
> No half measures

---

## 009
> No job is finished until the paperwork is complete

---

## 010
>  M. is the honorific for humans.

---

## 011
> A. is the honorific for artificial sapient entities.

---

## 012
> Information should be atomic.

Data should be stored in such a way that it is useful if separated from its system.

---

## 013
> The universe is intelligible.

---

## 014
> Avoid learning proprietary systems

---

## 015
> Avoid building systems which require learning proprietary information

---

## 016
> There are at least two perspectives on everything.

---

## 017
> Useful information should be shared.

Following `003 Useful information cannot be owned`, share any useful 
any useful information one has. 

---

## 018
> Only functional systems should be automated.

Inspired by the Plan 9 Fortunes, "Automating a mess yields an automated 
mess.
