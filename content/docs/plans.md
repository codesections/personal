+++
title = "My Plans"
description = "A list of things I plan to do."
+++
### Short Term (this week)
- **Learn** how to make a [Gutenberg](https://getgutenberg.io) theme.
- **Write** _Bootstrapping an MVP with OKRs & TDD_.
- **Write** _Build More Effective Documentation: Write Your Examples First_.
- **Release** a fork of Gutenberg with [Netlify CMS](https://netlifycms.org) pre-installed.

### Coming Soon (this season)
- **Develop** a boilerplate Python3 project & development environment for rebooting [qtMUD](https://qtmud.rtfd.io).
- **Write** casting-call for _The Delvers_ Season 1.
- **Learn** whether Sphinx, Gitbook, or Pandoc would be better for [Holistic Project Management](https://emsenn.gitlab.io/holistic-project-management).

### Long Term (within a year)
- **Re-release** Aperte, a single-file blogging engine tinier than the GNU Public License.
- **Re-release** Psalm to the Moon, my poetry book.
- **Record** _The Delvers_ Season 1, which will be a satirical fantasy podcast.
- **Code** the [U.S. Web Design System](https://designsystem.digital.gov/) as a theme for the [Gutenberg static site generator](https://getgutenberg.io).
- **Develop** _You Are Conscious_, which will be an interactive ficiton idleclicker in which you play an artificial intelligence tasked with making widgets.