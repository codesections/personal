+++
title = "Psalms to the Moon"
description = "Short Poems for a Bad Relationship"
sort_by = "weight"
+++

Psalms to the Moon is a book of short poems I wrote and released in 2015. It is currently available on Amazon in [digital](https://www.amazon.com/dp/B01BA3OH8G/) and [paperback](https://www.amazon.com/dp/1536980919/) format. I am beginning to copy them here, but this is _incomplete_.