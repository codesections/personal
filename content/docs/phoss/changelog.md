+++
title = "Keeping a Changelog"
description = "A PhOSS CHANGELOG is written for humans, by humans, reinforcing semantic versioning & a linear development flow."
+++

_Sorry, you dove too deeply into my website and found something I haven't had time to write._