+++
title = "Defining PhOSS"
description = "A definition of what it means to be a 'philosophically' open-source project."
+++
_Share, modify, and collaborate._

!!! info "Summary"
    Open-source software has traditionally been defined as software with a license that lets anyone view and change the source. However, much of the strength of open-source is in the collaborative culture it encouraged. The following definition hopes to capture that.

!!! warning
    This is a personal definition: something I've come up with describe my own software. I am not a philosopher, nor am I much of a developer.

!!! abstract "Implemention"
    Being philosophically open-source is more than just adding a badge to your project. It's a pledge to hold your code to a robust standard. Your repository must include a `CONTRIBUTING.md`, `CODE_OF_CONDUCT.md`, `LICENSE`, `README.md`, and `CHANGELOG`, all at the root level.

!!! tip "More Info"
    Follow or contribute to development at [http://op.emsenn.net/projects/phoss/](http://op.emsenn.net/projects/phoss/) or by contacting [emsenn](mailto:emsenn@emsenn.net). 

```
"Philosophically Open-Source" Definition v0.1.4
1. Encourage Free Distribution
The license should be the most freeing available (currently the Unlicense),
and the software should be packaged for distribution through common package
repositories. https://unlicense.org/

2. Encourage Collaboration
The software must include guidelines for contributing bug reports & patches,
and have provisions that work against discrimination.

3. Encourage Derived Works
The software must include instructions for creating a fork of itself.

4. Open-Source Stack
All dependencies of the software must be open-source as defined by the
Open-Source Initiative. https://opensource.org/osd
```