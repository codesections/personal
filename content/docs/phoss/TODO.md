+++
title = "Keeping a To-Do List"
description = "A clean way to keep a list of upcoming tasks."
+++

## Overview
A simple `TODO.md` file can be kept by listing all known upcoming tasks as directive sentences separated into three categories:
- _**Short-Term**_, for tasks you'll be working on this week.
- _**Soon**_, for tasks you'll be working on this month.
- _**Long-Term**_, for tasks you'll be working on this year.

_(Rather than time periods, tasks could be categorized by development milestone, such as the upcoming version, which is especially useful if using [semantic versioning](https://semver.org) and a [CHANGELOG](https://emsenn.net/docs/phoss/changelog/))_

## Example
```markdown
# To-Do
This to-do list follows the 
[PhOSS format](https://emsenn.net/docs/phoss/todo/).

## Short-Term (this week)
- **Learn** the TODO.md format.

## Coming Soon (this month)
- **Use** the TODO.md format for one of your small projects.

## Long Term (this year)
- **Teach** someone else to use the TODO.md format.
```

