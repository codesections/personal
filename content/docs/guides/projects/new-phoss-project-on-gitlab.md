+++
title = "Making a New PhOSS Project on Gitlab"
description = "Instructions for starting a new philosophically open-source project on Gitlab.com"
+++
# Project Initialization

This documentation walks through the steps to create a basic Git repository which pushes to a Gitlab.com project which is linked to an OpenProject project. 

## Step-by-step

1. Create a new local directory.
2. Initialize it as a git repository.
3. Populate it with relevant boilerplate.
4. Edit the boilerplate as needed.
5. Commit your changes to the master branch.
6. Create a new project on GitLab.com.
7. Within that Gitlab project's permissions settings, disable Container Registory, Issues, Pipelines, Snippets, and the Wiki. _(It is recommended to disable Pipelines and Container Registry as well, re-enabling them when necessary.)_
8. Push you local repository to that Gitlab project.
9. Create a new standard project in OpenProjet.
10. Add yourself as a member to that OpenProject project as a project administrator.
11. Set yourself as the project's responsible member.
12. Turn on these modules: Activity, News, Repository, and Work Package Tracking. _(It is recommended to disable all other modules, re-enabling them when necessary.)_
13. Add the link to the Gitlab repository within the OpenProject project's repository settings.
