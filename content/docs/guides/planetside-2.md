+++
title = "Personal Planetside 2 Guide"
description = "This is for me and my buddy Tom, but I'm pleased if you find a use for it."
+++

### Checklists
#### Starting the Game
- Open your map with `M`.
- Cycle through every menu on the bottom row, waiting until they load. _(This helps them load faster when you need them.)_ 

### Radio Behavior
#### Transmitting
When you have something to say, and are on a channel with > 2 people, open the channel & say their username (or approximation), followed by `This is...` and your own name, followed by, `over`:
> Iguana this is Toaster, over.

**If there's radio chatter and you want everyone to shut up, lead your message with a firm `Break, break!`**

After the person you're addressing has acknowledged receipt of your introduction, you may give your next message.

Other rules:
- Always say numbers by the digit. 1914 is `one-niner-one-four`
- Spell things out with the NATO phonetic alphabet
- If you know the thing bears repeating, preface your reptition with `say again`
- Give directions by compass direction if everyone is on foot, or by "bearing & yankee" if in vehicle, where bearing is angle from facing stern along a horizontal axis, and yankee is height (y-axis), divided into chunks of twelve. (Like a clock). This is the only time you get to say `ten`, `eleven`, `twelve`, instead of `one-zero`, `one-one`, `one-two`.

#### Receiving
When you hear a message addressed to you (or your squad, if you're squad leader), give acknowledgement of receipt with one of the following, in order of preference
- `Romeo`
- `Received`
- `Acknowledged`
- `Copy`

After you've received each message, you again acknowledge receipt. If the message is instructions, you may respond with `wilco` for "will comply". If the message is unclear, say `repeat`.

#### Example conversation
_(Iguana is piloting an airship while Toaster is gunning it)_
> Iguana this is Toaster, over.
>
> Toaster, Iguana romeo.
>
> Iguana, bring us to bearing two-four-zero.
>
> Toaster, wilco.
>
> Iguana, any plans for the weekend af-
>
> Break, break! Hostile at bearing seven yankee ten.
>
> Toaster, iguana, repeat.
>
> Hostile at bearing seven yankee ten, repeat seven yankee ten.