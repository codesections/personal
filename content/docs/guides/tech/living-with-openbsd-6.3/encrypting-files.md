+++
title = "Encrypting Files"
+++

I will save y'all the trouble of hearing about my search for this answer, but when you want to encrypt a single file or directory with a keypair (not password) in OpenBSD, you'll want to use GnuPG. And if you're using `pass` (aka _password-store_), you'll need it.

`doas pkg_add gnupg` asked which version I wanted, 

```
Ambiguous: choose package for gnupg
        0: <None>
        1: gnupg-1.4.22p1
        2: gnupg-1.4.22p1-card-ldap
        3: gnupg-2.2.4
```

I went with #3, simply because it's the highest version number.

The first task (and only one I need to do right now, since I'm just setting up `pass`), is generating a keypair. Skimming the man pages for `gpg2` tells me the command I want is `gpg2 --gen-key`, which will then walk me through the rest of the process.

After I go through the survey, it tells me to type or move the mouse to generate entropy. Now _that_ makes me feel proper sci-fi, but before I have much opportunity to do anything, it finishes up, and asks me for a passphrase for the key.

Done! I don't have to encrypt any files rn, or do anything else with gnupg, so that's the end of this documentation for now.
