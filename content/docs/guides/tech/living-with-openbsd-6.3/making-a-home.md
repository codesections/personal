+++
title = "Making a Home"
+++

This is part of my [Living with OpenBSD](living-with-openbsd-6.3) series. I should lead by saying I'm in no means an expert on this topic - in fact, I may be less knowledgeable than you. This document is primarily for my own reference.
