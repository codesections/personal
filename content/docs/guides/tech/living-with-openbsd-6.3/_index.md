+++
title = "Living with OpenBSD 6.3"
description = "It's one thing to use an operating system, it's another to live your life with it."
+++

It is April 26th, 2018, and I have been transitioning to using OpenBSD as my primary operating system for about two weeks.

The reasons for the transition are outside the scope of this text, but I want to state that they are largely philosophical, and I cannot offer any technical justification for it. There are certainly those out there that can, but, that's not why I'm switching.

I also regretfully need to inform you that I did not think to begin writing these documents before I started on the project. Already, information I know I'll want later has been lost to the fuzz of naive debugging. Hopefully, I'll be able to fill in the blanks but for now I can only offer my apologies.