+++
title = "Password Management"
+++

This is the first document I'm writing about using OpenBSD as my primary system, so apologies if it's a rough read.

---

At the moment, I use LastPass for managing credentials for clients, and BitWarden for my own uses. My normal process is that when I make a new login, I add it to the respective vault - easy enough to do if it's a website, but not really useful for commandline credentials, or non-password things. I also have the problem of clutter - passwords aren't sorted when I add them, so I have to make a dedicated activity of sorting new credentials. Since I onboard and offboard multiple clients a month, it isn't an uncommon task.

Since I've been generally re-evaluating how complex the tools I need to use should be, my first thought is, can I accomplish what I want without any outside tools. This led to me thinking more critically about what I wanted: a file containing a single credential (username, password, other relevant info), stored inside directories & subdirectories. With useful filenames, I can do any sort of processing of that with the system default tools to get the credentials (or parts of credentials) I need.

While I'm sure there's some flaws with that approach, it seems quite "Unix-y" to me. Of course, it's horribly insecure, but that's nothing some keyed encryption can't handle - the directory structure could be wrapped itself, if wanted.

At this point, I think I have the solution I'll want to proceed with. I think at this point I'm beginning to understand the command line well enough that I'd be able to construct a passable version of what I need without looking past the `man` pages for help, except possibly the encryption; I'm entirely unfamiliar with the tools available to me in OpenBSD for such tasks.

However, I don't wish to reinvent a wheel, so now that I know what solution I'm looking for, I search on the Internet for existing programs that match that solution.

One of the results is for `pass`, also called `password-store`. It bills itself as the "standard UNIX password manager", and seems to work about how I outlined above.

Running `doas pkg_add password-store` installs the package with no apparent issues (beyond a complaint I don't have a folder for bash completions, which makes since as I'm using ksh.)

The instructions say to run `pass init <gpg-id>`, where gpg-id is the ID of my GnuPG encryption key. Except, I don't have one on this computer.

See [Encrypting Files with OpenBSD 6.3]()

Once I finish that, I run `pass init emsenn` which seems to work without issue. One of the cooler things about _password-store_ is that it integrates in with git. I'm transitioning to using _fossil_ as my own DSCM, but I figure I'll stick with _git_ here, as long as it is already integrated.

So I run `pass git init` and... I'm done. I could set a remote repo to push to, but right now I'm not sure where that would be.

---

The last step is to move my existing credentials into _pass_. I could use a migrationn tool, but honestly... my credentials inside my current tools are a mess. I'm going to manually copy them, verifying they don't work along the way.

As an example, here's how I inserted (that's the verb _password-store_ uses for "add an existing password) my Streama credentials:

```
pass insert LAN/Streama/emsenn
```

I paste my password twice upon request, and it adds it to the password store (and creates a git commit for the new password). 

To make sure it works, I type `pass LAN/Streama/emsenn` which should spit out the password, after asking for the passphrase I used when I generated my GnuPG keypair. It does.

---

 
