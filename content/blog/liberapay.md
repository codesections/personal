+++
title = "Looking at Liberapay"
description = "Liberapay is a recurrent donation platform like Patreon, but more closely aligns with my open-source values."
date = 2018-01-28
+++

As part of my [ongoing overhaul](/post/i-love-enabling-collaboration/) of my [digital ecosystem](/post/digital-ecosystem-2018/), I was looking for different ways to increase my revenue.  Specifically, I was looking for ways to monetize doing what I already do, and ways to make it easier for people to donate money to me.

From my time consulting with musicians, I was well aware of [Patreon](https://www.patreon.com/), where you could solicit for recurring donations from your supporters.  But a big part of Patreon was giving early or exclusive access to your content to your supporters, and that contradicts my desire to be [philosophically open-source](/post/philosophically-open-source/).

---

[Liberapay](https://liberapay.com/) does not.
- They're [open-source](https://github.com/liberapay/liberapay.com).
- They're [run by a non-profit](https://github.com/liberapay/liberapay.org).
- They don't take a percent of your revenue.
- They're explicitly for people who contribute to the commons (building freee software, spreading free knowledge).

I've just created my account, and it was pretty simple.  You pick whether you want donations in USD or Euro, set an optional goal, username, and statement, and... that's about it.

I've set my weekly goal there to $3.25, which is my current cost for webhosting all my personal stuff, plus a little bit.

For now, you can get to my profile [directly](https://liberapay.com/emsenn/), but soon I'll probably add it into this blog.

If you're looking at using something like Patreon, I definitely encourage looking at Liberapay instead.