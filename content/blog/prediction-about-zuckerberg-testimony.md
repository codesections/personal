+++
title = "A Prediction About Zuckerberg's Testimony Before Congress"
description = "The next move is establishing regulations that formalize an Internet hegemony in the name of protecting the state."
date = 2018-04-11
+++

Zuckerberg & Congress are putting on a show to justify setting up regulation which will strengthen the connection between our state, data carriers, and data service providers.

The result of these hearings will be the establishment of new regulation, that will build on the framework our government has been building for the past decade, to formalize an Internet hegemony where service providers share data with data carriers, who in turn share it with the government.

Alternative service providers will be unable to meet the burden of these new regulations, as demonstrated already with the shutdown of services in response to the passage of SESTA/FOSTA.

We will see a further enclosure of our personal data into centralized siloes, with the justification that it is necessary for our privacy, while we continue to progress into a future where data is one of the principle elements of commerce.

Rentseeking has been an issue (or feature) of society through history. Now it has become an enshrined & codefied part of the Internet: we need everyone to be a perpetual customer.

---

This post isn't a call to action, or presenting any alternatives. This is a PSA: This is the Internet now; the regulations are in place & the economic wheels have been greased.