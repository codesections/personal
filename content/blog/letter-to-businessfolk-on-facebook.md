+++
title = "A Letter to Businessfolk on Facebook"
description = "I understand an appeal to morality isn't enough. This is an appeal to your bottom line."
date = 2018-04-27
+++

> "TBH, I don't really care about the morals of websites I use, I'm just here for the audience."

If you're in my Facebook audience, you're probably earning at least part of your income from Internet marketing, and that probably means a fair chunk of it comes from Facebook. So the idea of leaving Facebook has little to no appeal - I get it.

But think back. Who were the first people on FB? Nerds at Harvard. And then nerds from other universities. These early adopters set the tone for the platform, and had marketing opportunities simply unavailable to those who came later - after all, they weren't marketers, they were just another early adopter, checking out the viability of a new product.

And the audience they had. Holy crap. Full of the sort of people who are not just looking for the next big thing, they're building it. For most of you, this means "any lead from this source is halfway qualified" - can you say that about any other free lead source?

If you aren't looking at these emergent markets, you're turning away from a valuable resource, and even though you might not care about the "politics" around Facebook, you probably care about free leads.