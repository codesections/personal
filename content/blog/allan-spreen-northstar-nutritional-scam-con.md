+++
title = "I Found a Medical Scam Piggybacking off Anti-Clinton Propaganda"
description = "Dr. Allan Spreen of North Star Nutritionals is misrepresenting himself & his products."
date = 2017-12-17
author = "emsenn"
category = "investigation"
+++

A doctor is misrepresenting himself and his research to popularize a theory that Hillary Clinton has conspired to hide cancer cures for decades.  This is criminal, and below, I expose it.

<!-- more -->

Today, across the conservative web, there has been a video being shared as a banner ad, embed, or whatever.  Here it is as a lander page I saw under a Fox article about the upcoming Mercedes C-Class.  So, since I doubt any of you want to take the time to watch it, the video claims to be a Dr.  Allan Spreen, explaining how Crooked Hillary is in bed with the hospital cartels to encourage cancer so she could have dead people vote for her in the election.  Again, the theory is, Clinton has been working with medical companies for two decades, to encourage cancer, so there would be dead people she could have vote for her.

At its face, the theory is a hybrid of some modern anti-pharmaceutical rhetoric with some of the old-school anti-Clinton jabs from back in the Cattlegate days.

Well, the video actually is someone reading an abbreviated version of an article written by someone who claims to have spoken to Dr.  Allan Spreen.  Now wait - the video said it was Dr.  Spreen, and this "article" says they spoke with Dr.  Spreen? Why would Dr.  Spreen read, nearly verbatim, something written by someone else?  The video I link *is* Dr.  Spreen, you can compare his voice with other videos he has on YouTube.  Therefore, it seems likely the article was written by Dr.  Spreen, and he misrepresented himself to avoid violating medical laws.  (Doctors aren't allowed to just make cladims about products as doctors, because that's reckless.))

Anyway, let's move past that and look at Mr.  Not-Dr-Spreen's blog post.

Reading the full article, it's...  it's long.  And rambling.  But is actually a fucking sales letter, once you get to the en.  For "EG-M", which the sales letter says "holds mega amounts of Collagen, plus more then [sic] a dozen amino acids, essential proteins, and healing minerals..."

Interesting, Dr.  Spreen, the scientist who has done all of the not-peer-reviewed research on EG-M's efficacy as a treatment for the various illnesses Mr.  Not-Dr-Spreen shares with us, owns a company which sells a variety of products which claim to contain EG-M! Boy, it sure would be suspicious if Mr.  Not-Dr-Spreen and Dr.  Spreen knew each other, because it would really make a lot of this written seem bias!

Anyway, they don't, clearly, so let's look at EG-M, and see if we can't suss out the truth to these claims ourselves.   Not that I don't believe Dr.  Spreen & Mr.  Not-Dr-Spreen, but, you know, I want to do my own research.

EG-M...  EG-M...  hmm, you know, no where in either of their writing do these two highlight what EG-M is.

But, there is this thing, endothelial growth medium, which is abbreviated as EG-M, and it is for cultivating cells that would grow on the inside of blood vessels...  which would be mostly collagen, plus about a dozen amino acids, some proteins, and mine- oh.

Oh, I...  I see what's happening.  A doctor is lying about his identity online to popularize a belief that Hillary Clinton has been working with pharmaceutical companies to suppress a cancer treatment that he directly profits from.

{% admonition(title="Update") %}
I filed complaints with relevant regulatory agencies but as of July 2018 received no response beyond confirmation of receipt.
{% end %}
