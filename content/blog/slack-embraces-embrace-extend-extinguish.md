+++
title = "Slack Embraces 'Embrace, Extend, Extinguish'"
description = "With the closure of their XMPP and IRC gateways, Slack has moved on to the 'extinguish' phase."
date = 2018-03-10
+++

Slack is the latest company to embrace the strategy, made famous by the Department of Justice in the antitrust suit against Microsoft It boils down to this:

- Embrace: Develop software compatiable with competing products, or implement an open standard.
- Extend: Add features not supported by the competition or included in the standard.
- Extinguish: When the featureful iteration is the de facto standard due to market share, remove compatibility.

---

Slack has recently closed their XMPP and IRC gateways, walling in their garden substantially. As if to drive home the point, reading the announcement requires a Slack account, but the lede is:

"Saying goodbye to Slack’s IRC and XMPP gateways.

"As Slack has evolved over the years, we’ve built features and capabilities — like Shared Channels, Threads, and emoji reactions (to name a few) — that the IRC and XMPP gateways aren’t able to handle. Our priority is to provide a secure and high-quality experience across all platforms, and so the time has come to close the gateways."

---

Slack is another platform that doesn't do what you think it does, folk. It doesn't let you safely own your data, or communicate easier. It makes you pay rent to do what is one of the simplest things there is online.