+++
title = "Goodbye, Sam Jones"
description = "From biscuits to bickering, it's been swell."
date = 2018-02-18
+++
In the early spring of 2014, I moved into a ranch house outside of town. Bigger than I needed, the house gave me room to host friends' band rehearsals, house shows, and perhaps most importantly, meetings.

The house was on Sam Jones Road, and so quickly became the Sam Jones House. I became known for always having an open door, with fresh biscuits & coffee available for those who stopped by. It was, I think, a good combination of what I'd learned about anarcho-syndicalism from my time at Occupy with true Southern hospitality.

And it was immediately successful. Despite being quite a drive for many of them, there was usually at least someone at the Sam Jones House, taking advantage of the environment to help produce their art. It was, looking back, far more successful of a project than I'd expected it to be.

Then, that summer, I was in a severe auto accident. It led to me needing a wheelchair for about a year, which meant living at the Sam Jones House wasn't going to be feasible. (Older house meant my wheelchair couldn't get through the doorways.)

Things the Sam Jones House helped enable continued, but with me in recovery, the scope changed, and by the time I able to get around on crutches, I'd found that the local scene had largely moved on. This, with other factors, soured my working & personal relationship with a lot of the local musicians and artists.

I wasn't finished, though. Lacking the house, I re-branded to Sam Jones Media, and released a new mission statement: I wanted to help artists publish, distribute, and monetize their original content without giving up ownership rights. I stopped focusing local and started focusing on folk who were also outsiders in their own artistic community.

Since that refocus in early 2015, I've been able to help more than a dozen writers get self-published, enabled the release of countless albums, and helped several visual artists prepare themselves for gallery showings. Like the Sam Jones House, Sam Jones Media was far more successful of a project than I expected it to be.

Now, it's time to shut down Sam Jones Media. Things have changed since I got started. Revenue for artists in my local area continues to drop thanks to the efforts of former colleagues like the Three Corners Collective. Tax codes have changed so I can't as easily write off Sam Jones Media expenses. And perhaps most importantly:

There are smarter people working harder to accomplish the same thing. With the recent innovations in decentralized publishing, distribution, and monetization, the kind of services Sam Jones provided just aren't as necessary. While once it might've taken me walking someone through the steps to prepare a book for Kindle publication, there are an abundance of free tools. And if you need more information, there are professionals more able to help you than I would be.

Most of Sam Jones Media is just being reworked to be a part of my personal brand. Release systems like Stantia and Aperte will become available again soon, and the day-to-day management of these funnels will be integrated with my other professional services.

The values that led me to create Sam Jones, haven't changed. I still think artists should own what they make, and I still think that by working together they can repair their economic market.

The only thing that's changed is that it is me saying all this directly, instead of a brand.