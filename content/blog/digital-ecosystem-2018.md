+++
title = "Digital Ecosystem 2018"
description = "How I get things done using tools that support my philosophy."
date = 2018-01-28
[extra]
suppress_meta = true
+++

> This post has been abandoned. I change what software I'm using way too 
> often to keep up with it here.

Recently, as part of an [ongoing overhaul](/post/i-love-enabling-collaboration/) of my digital life, I've been considering what I've taken to calling my _digital ecosystem_: the software & tools I use to do things on my computer.  I've been working to define what the "perfect" software solution is, and then seeing where I can improve the software I use.

In my opinion, the perfect software is:


I'm trying to do a better job of making sure that the products I use work with my personal philosophy. Obviously, I can't sum up the whole of what that means, but I've tried 

- **Open Source**. The more freeing the license, the better.
- **Private**. Self-owned & encrypted is better than not.
- **Decentralized**. If it requires outside communication, do it without a central carrier.
- **Browser-Based**. Accessible through standard protocols & interfaces.

I plan on writing more posts explaining my choices, but below I'm including a list of some of the software I'm using, and why I'm using it:

## My Digital Ecosystem, 2018


---
**[A-Ads](https://a-ads.com)** _...instead of AdSense._
- Easily use multiple ad codes per site.
- Get paid directly in BTC.

---
**[Amazon Lambda](https://aws.amazon.com/lambda/)** _...instead of Zapier._
- I like Python, being able to write Python to do _stuff_ online is great.
- Pay for the computing I use, not a monthly subscription.

---
**[Amazon EC2]()** _...instead of dynamic webhosting._
- Pay for the hosting I use, not what I might (won't).
- Easily deploy images for webapps I want.

---
**A

---
**[AWS](https://aws.amazon.com/https://aws.amazon.com/)** _...instead of other webhosts & mail servers._
- Pay-for-what-you-use.
- Popular among open-source folk.
- Probably how all hosting will work in the future, may as well understand it now.
- **Not** open-source. :-(

(Right now I use EC2 to host dynamic sites, SES to host my mail servers, and S3 to host my static sites.)

---
**[Cryptpad](https://cryptpad.fr)** _...instead of Google Docs._
- Autosaving rich-text & syntax-highlighting editor.
- Encrypted, but still easy to share with others.
- Self-hosted, but also SaaS (software-as-a-service).
- Real-time collaborative editing.
- Make slideshows (like this one) in Markdown.

---
**[Earn.com](https://earn.com)** _...instead of Fiverr._
- Block-chain integrated.
- Higher quality users.

---
**[GitLab.com](https://gitlab.com)** _...instead of Github.com_
- Version-controlling code repository with social elements.
- Allows for free private repos.
- Built-in CI/CD cuts down my workload.

---
**[Gutenberg](https://www.getgutenberg.io/)** _...instead of Jekyll (sometimes)._
- Single-binary static-site generation.
- Very fast.
- Very open-source.
- Gives me an excuse to learn Rust.

---
**[Jekyll](https://jekyllrb.com/)** _...instead of Wordpress._
- Much much simpler.
- Static websites are cheaper to host.
- Lets me write my blog like I write code.
- Big community.
- Forces me to use Ruby.

---
**[Matrix](https://matrix.org)** _...instead of Slack._
- Decentralized.
- End-to-end encryption coming soon.

---
**[Odoo](https://odoo.com)** _...instead of a CRM, mass-mailer, project manager, timesheet tracker, and more._
- Literally almost everything a small or medium business would need, in a box.
- Seriously, super powerful software, for free.
- Like, everyone I've shown this to has been amazed.

---
**[Steemit](https://steemit.com/)** _...instead of reddit._
- Blockchain-integrated news aggreggate.
- Get feedback on your curation, not just posts.
- Feedback can be cashed out to other currency.

---
**[Vivaldi](https://vivaldi.com/)** _...instead of Chrome._
- More powerful without extensions
- But also works with Chrome extensions
- Uses less memory.
- Honestly just more aesthetic.


---
### Coming soon...
- Static site hosting with [IPFS](https://ipfs.io/)
- Blockchain stuff
