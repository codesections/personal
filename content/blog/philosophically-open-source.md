+++
title = "Defining 'Philosophically Open-Source'"
description = "To be philosophically open-source, a product should not just allow, but encourage free distribution & modification."
date = 2018-01-28
+++

> The updated definition of "PhOSS" lives at
> [https:/emsenn.gitlab.io/docs/phoss/](https://emsenn.gitlab.io/docs/phoss/).
> This blog post is the precursor to that page, and is out of date.

Whenever I go to tell someone about one of the many [projects](/projects/) I'm working on at any given time, I almost certainly will toss out the phrase "philosophically open-source".

Defining what exactly that means is a bit trickier than just saying it.  Let's look at what makes a project _open-source_, and see how that contrasts with one that is _philosophically open-source_.

The Open Source Initiative has a pretty good [definition of open-source](https://opensource.org/osd), which I'll excerpt from below:

> **1. Free Redistribution** - The license shall not restrict any party from selling or giving away the software

So, the thing has to be free like "free drink with purchase".

> ...The program must... allow distribution in source code as well as compiled form...

The code behind the thing has to be available.

> ... The license must allow modifications and derived works...

Other folk are permitted to make a new thing out of your thing.

---

There's some more specifics in the actual definition, and some new additions about how you must not discriminate, either against individuals or certain fields.  But what I want to highlight is the recurring little phrase "**must allow**".

To be open-source, a product **must allow** for free distribution & modification.

To be _philosophically_ open-source, a product **must _encourage_** free distribution & modification.

It isn't just enough to allow someone to improve the source code - you should encourage it, by providing systems & channels to assist them.  Which means that as a developer, releasing a philosophically open-source product means not just agreeing to release the source code, but agreeing to serve as the product's manager.

---

As with almost anything where the word "philosophy" is brought in, defining the difference between "allow" and "encourage" can be a bit tricky.

But there are a few things that I think can be stated pretty concretely, giving us a good working definition of _philosophically open source_:

```
"Philosophically Open-Source" Definition
1. Encourage Free Distribution
The license should be the most freeing available (currently the Unlicense), and the software should be packaged for distribution through common package repositories. https://unlicense.org/

2. Encourage Collaboration
The software must include guidelines for contributing bug reports & patches, and have provisions that work against discrimination.

3. Encourage Derived Works
The software must include instructions for creating a fork of itself.

4. Open-Source Stack
All dependencies of the software must be open-source as defined by the Open-Source Initiative. https://opensource.org/osd
```