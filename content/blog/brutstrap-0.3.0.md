+++
title = "Announcing Brutstap 0.3.0"
description = "Brutstrap is a simple CSS framework for building brutalist websites"
date = 2018-07-11
+++

Last week, I read an interesting post by David Bryant Copeland about [brutalist web-design](https://brutalist-web.design/). Yesterday, I took some time to make myself a set of CSS components and modifiers that were inspired by his post. See, I loved what he had to say, but found it a little ironic that the was using [Tachyons](http://tachyons.io/) to style his page: it seemed like overkill.

What I made, I'm calling [Brutstrap](https://emsenn.gitlab.io/brutstrap), a portmanteau of "brutalism" and "Bootstrap", the very popular CSS framework built by Mark Otto and Jacob Thornton.

On down the line, I'd like to turn it into a [Gutenberg](https://getgutenberg.io) theme, and a [pandoc](https://pandoc.org/) template. But already, I was able to use the framework as a base for [one of my landing pages](https://emsenn.gitlab.io/okr-overhaul), which is already generating qualified leads!

Pretty exciting stuff, considering that I didn't know CSS or HTML at the start of the year.