+++
title = "CSS Themes Should Be Better"
description = "If your code is meant to be reused, make it better."
date = 2018-07-14
+++

I know I'm not a real web developers, but I really feel like if you're going to make something a theme, that's going to be used by more than one website, making it more strictly follow standards and stuff should really be prioritized. Like, I understand for a single site, fuck it, make the things pretty how you want. But if it's meant to be deployed even on a dozen sites, think about how that amplifies any shortcode or oversight in your layout.

I got to thinking about this because the existing Gutenberg themes - and themes for Hugo and Jekyll, cause I got to looking - honestly kind of suck on this front. Infrequent use of tags like `<header>`, `<main>`, so on. Using ID attributes for CSS, not generating them based on page front-matter.

And frankly I don't see there as being any excuse. It's trivial to type id="{{page.slug}}", or add in conditionals for ograph data. Just... do it. I'm a newbie, and I am.

---

I'm on a rant now so just ignore me if you want, but for real, no one with their priorities straight gives a shit if your website builds in 200ms or 2 seconds. They don't even really know what "build" means. They'll give you the inch of agreeing to use markdown to write their pages. They'll even learn to do weird {{ shortcodes }}. But then you have to make sure their images and descriptions show up right on Facebook shares, and they can drop in their SaaSy widgets.

This is why I'm so in love with Netlify at the moment - I haven't been using them long enough and I'm too ignorant to know about the tech, but their marketing really does a good job of hitting the market's pain points and resolving them. And what they're offering isn't much different than what a bunch of other folk offer. But it's different enough that they just fit into the market better.