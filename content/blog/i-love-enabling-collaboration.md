+++
title = "I Love Enabling Collaboration"
description = "At the heart of so much of what I love to do is one thing: making it easier for people to work together to create something new."
date = 2018-01-23
+++

I've recently thought that a lot of the projects I start have are, at their 
core, very similar. They are almost all about making it easier for people 
to contribute to & collaborate on projects of different sorts.  This thought 
led to me thinking of how I could explain this to people succinctly, since as 
much as I resist, I need to think about personal branding.

What I came up with was:

**I love enabling collaboration.**

I really do. And as I said, a lot of my projects dance around fulfilling that 
love in one way or another.  And what I love is bringing the ideas of 
open-source collaboration to things that aren't conventional software 
development.

---

Unfortunately, I'm quite distractable, which means that frequently, I move onto 
another project before I'm anywhere near done with one. Maybe that's part of 
the appeal in collaboration for me: the project can move forward thanks to the 
work of the other people involved, even if I get distracted.

That distractability is also probably what encouraged two other interests I 
have that relate to enabling collaboration: open-source, and good 
documentation.

Releasing my stuff as open source, and hosting the code in public repositories 
means that even if I move on from a project for a few months, or even a few 
years, I can still pick it back up when I come back around to the idea.

And writing good documentation means that I can pick it back up _quickly_, 
rather than having to hope I remember what I was thinking whenever I last 
worked on it.  Because, let's be honest, I won't.

---

Unfortunately (it seems I can start a lot of sentences about myself with this 
word), I've not been very good at living up to any of this.  I'm going to try 
to be better about it, in part by adding posts here that explain my progress 
and thoughts.
|
