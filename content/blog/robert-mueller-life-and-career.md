+++
title = "Robert Mueller's Life & Career"
description = "A brief summary of Robert Mueller's life and career."
date = 2017-12-15
+++

# Robert Mueller's Life & Career

> Disclaimer: Mueller was technically at the top of my chain-of-command for a brief period in 2011, though I was unaware of that when writing this.

Who Robert Mueller is will be a frequent discussion here soon. Here's a summary of his life & career, so you can sound smart in conversation. It may also persuade you against believing rumors that he has a "bad man" who wants to "destroy America."

He was born in 1944 in Manhattan but was raised outside Philidelphia. He was an athlete in high school, and after his grauation, he went to Princeton, where he earned an A.B. degree in Politics. In 1968, Mueller volunteered for the Marines, influenced by the death of a friend with whom he'd played lacrosse while at Princeton.

---

After enlistment, he attended officer training at Parris Island, as well as the Army Ranger and Army Jump schools. He served as a platoon commander in Vietnam, where he was shot in the thigh, but later returned to active duty. During his service, he earned a Bronze Star (w/ Combat V), Purple Heart, commendations from both the Navy and Marines, and the Gallantry Cross from the Republic of Vietnam.

---

When he returned to the United States, he attended Virginia Law School and earned a law degree. Afterward, he spent three years as an attorney in San Fransisco, before moving to the US Attorney's Office for Northern California. He was promoted to the chief of their criminal division before moving to Boston, to work as the Assistant United States Attorney for the District of Massachusetts.

After a stint there, he left to work as partner at a private legal firm, before joining the US Department of Justice to serve as assistant to the Attorney General and acting deputy Attorney General.

While working as the acting deputy Attorney General, Mueller oversaw the prosecutions of Manuel Noriega, the Lockerbie bombing, and John Gotti.

---

I'm skipping past the years of all this to keep this short, but everything from his graduation from Virginia to the prosecution of John Gotti was between 1976 and 1993.

After that, he moved back to the private sector, where he was again a partner at a firm in Boston, this time specializing in white-collar crime litigation. After a couple years, he returned to work as the senior homicide litigator for D.C.'s US Attorney's Office.

In '98, Mueller was appointed US Attorney for Northern California, where he worked until 2001, when George W. Bush nominated him for the position of FBI director. He was unanimously confirmed, and officially became the Director of the FBI on September 4th, 2001 - just before 9/11.

---

While serving as FBI Director, Mueller led the effort to curb domestic wiretapping, removed the FBI from participating in the CIA's enhanced interrogations, and more generally, but still worth noting, led the FBI through the years after 9/11, when we were forced to collectively ask a lot of questions about our intelligence community for the first time.

Mueller served as FBI Director for 12 years, a term long enough that it required explicit Senate Approval. In 2013, he was replaced by James Comey, and since that time has been teaching, speaking, and consulting... until he was asked to move back from the private sector, to help investigate our past presidential election.

---

Mueller played sports in high school. He volunteered to fight in Vietnam after a friend died. He's served in the public office for years but has also shown a keen interest in the private sector. But most importantly, he has around three decades of experience investigating white-collar crime, and despite having worked for employers from many parts of the political spectrum - and outside of politics entirely - has never been accused of any impropriety or bias.
