+++
title = "Announcing Homepage To-Do"
description = "A way to keep a pretty to-do list on your desktop."
date = 2018-07-15
+++

My [homepage](https://emsenn.net) currently shows my personal to-do list, which is rendered from a markdown file in my home directory courtesy of the [Gutenberg static site generator](https://getgutenberg.io).

A colleague really enjoyed it, so I threw together a way he could keep a similar To-Do list on his desktop. Below is an embed of a demo of what the page looks like - obviously in practice, it'd be full-screen.

<iframe width="100%" height="auto" src="/homepage-todo-clean.html"/></iframe>

It's a single HTML file, _but_ you write your to-do items in Markdown, thanks to [StrapdownJS](https://strapdownjs.com).

There are two versions available:
- [One with an included explanation](/homepage-todo.html)
- [One that's clean](/homepage-todo-clean.html)