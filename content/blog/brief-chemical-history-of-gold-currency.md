+++
title = "A Brief Chemical History of Gold Currency"
description = "Going through the periodic table, gold is basically the only possible thing to use for pre-historic currencies."
date = 2016-11-30
+++

A while ago, someone asked me why we value gold, and why did we use it as currency for so long?  As I'm wont to do, I wrote up way too long of an explanation, and I thought I'd share it with y'all.  But basically, gold is the only thing we could use as currency, reliably, for a very long time.

Gold is a pure element, and its purity is easy to test.  Assuming an early currency-alike would need to be elemental (which it does, because making any sort of stable alloy is complex and took us several hundred years to sort out, at our best), let's look at the periodic table: There's a bunch of gases, so nope.  Then we have some things that are liquid at room temperature, like mercury and bromine, and both of those are toxic.  We've got some other toxic things like arsenic.

Almost everything on the left half of the table is too reactive - a currency that explodes when it gets wet isn't much use.  At the bottom we have radioactive chemicals, and while we didn't know why we didn't want to hold them on our person for years, we seemingly figured it out, plus their scarcity make them unfeasible unless paired with another, more stable element (and then why not just ditch the radioactive bit?) You've got your "rare earth" elements, but most of those are nearly indistinguishable without modern technology, so they don't work.  So now, we've got the center of the periodic table left - transition and post-transition metals.

There are 49 of these: iron, titanium, copper, lead, aluminium, so on.  Some, like titanium and zirconium, are very difficult to smelt, requiring modern techniques to produce.  Others, like iron and copper, aren't stable and readily oxidize.  A currency that melts itself away is hardly useful! Of the 118 elements, we've got...  platinum, rhodium, iridium, osmium, palladium...  a couple I'm forgetting, and gold and silver, that aren't ruled out for being gaseous, liquid, unstable, or requiring modern science to create and verify.  These are the "noble" metals, because they barely react with the other elements.

All of these except gold and silver are VERY VERY rare, and most require, again, modern metallurgy in order to process...  all of them except gold and silver.  Silver reacts with sulphur in the air, so isn't ideal as a currency, leaving...  gold, which works because it is literally the most boring element (which, fun fact, is also why it's so shiny, but that's a different thing.)

It's also sufficiently rare, yet common enough globally that multiple societies independently arrived at it as a currency.  Also, it's...  golden, whereas all the other metals are silvery, or green once they begin to degrade.  So it's exceedingly easy to recognize, and very hard to mimick (in fact, we don't know if anyone prior to the modern era made strictly "fake" gold coins; counterfeit gold coins would have been alloys of lower purity than the normal currency, not a different metal entirely.)

So yeah.  We used gold for currency for so long because it was literally the only option, because science.
