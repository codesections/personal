+++
title = "emsenn's blog"
sort_by = "date"
+++

{% admonition(title="Summary") %}
This is my blog, where my shorter, more casual, or more timely writings go. Below, you can see the posts sorted chronologically.
{% end %}
