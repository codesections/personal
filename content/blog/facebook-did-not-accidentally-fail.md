+++
title = "Facebook Did Not Accidentally Fail"
description = "Despite their claims, recent events involving Facebook aren't an accidental consequence of a new platform."
date = 2018-04-11
+++

In 2009, as the world was going through a recession, Russian social-media mogul Yuri Milner invested $200m in Facebook, w/ a valuation of $10b, w/o requesting a seat on the board OR voting rights.

The investment was financed by Alisher B. Usmanov, who provided funds from Gazprom, a Russian natural gas-and-more conglomerate. It's likely the investment is what pushed out Facebook's early CFO Gideon Yu.

The investments were facilitated by Ryan Williams, a friend and former classmate of Jared Kushner. At a similar time, Yuri Milner also invested in Cadre, a company founded by Williams, Kushner, and Kushner's brother, Josh Kushner. However, Jared Kushner failed to disclose his ownership of the company.

Shortly after, Milner & Zuckerberg became close associates, meeting monthly, even speaking together at events. In 2012, Milner attended Zuckerberg's wedding, and in 2014, paid 100% above value for a home in California.

---

Alisher B. Usmanov spent 6 years (1980-1986) in a Soviet prison for fraud and embezzlement, before becoming a manager of Gazprom-owned steel mills. In 2008/20009, he fired the editor at one of Russia's most-respected newspapers for an article detailing Russian electoral fraud. The article said, "[Usmanov's] ties to the Kremlin and Facebook have stirred concerns that he might influence the company's policies in subtle ways to appease governments in markets where Facebook is also an important tool of political dissent, such as Russia."

Usmanov is known to be a close friend of Roman Abramovich, whose wife during this period was in turn close friends with Ivanka Trump, thanks to Wendi Deng. This led to Ivanka attending a lunch hosted by Lev Leviev, an associate of Abramovich's, where Deng introduced her to Jared Kushner. (Concurrently, Leviev was hosting the bris for the grandson of Tamir Sapir, who's daughter Zina Sapir married Africa-Israel CEO Roten Rosen. Trump, a few months earlier,, had hosted the wedding of Sapir and Rosen.

The Sapir Organization, with the Trump Organization, built Trump SoHo. (Which was an absolute scandalous mess, not going to get into that here.) They attempted to mimic the process with Trump Tower Moscow, as well.

---

Gazprom - the conglomerate which both Abramovich and Usmanov have been senior executive at, and which provided the funds for Milner's investment in Facebook - may be familiar to you because of their recent mentions in the news. They are the financiers of the spy ring which, in 2013, tried to recruit Trump advisor Carter Page.

---

When Facebook was first confronted about the "fake news" epidemic and its influence on our election, Zuckerberg said it was "crazy." In April 2017 when they put out a report on the issue, they left out any mention of Russia.

Political ads were paid for in rubles. Facebook had "embeds" working directly with the Trump campaign.

"[People from Facebook, Google, and YouTube] were helping us... they were basically our hands-on partners... ...Without Facebook, we wouldn't have won." - Theresa Wong, Trump campaign.

"We found that Facebook and digital targeting were the most effective ways to reach the audiences. After the primary, we started ramping up because we knew that doing a national campaign is different than doing a primary campaign. That was when we formalized the system because we had to ramp up for digital fundraising. We brought in Cambridge Analytica. I called some of my friends from Silicon Valley who were some of the best digital marketers in the world. And I asked them how to scale this stuff." - Jaren Kushner.

Cambridge Analytica is the firm who coordinated with Julian Assange about Hillary Clinton and the DNC's stolen emails. Michael Flynn, who has pleaded guilty to lying to the FBI, was an advisor at Cambridge Analytica.

---

Facebook has early investment ties to Russian financiers and openly worked with them for years. The Trump family has ties to those same people. Facebook then worked directly with the Trump family to implement the methodology of those people.

It was not an accidental failure of a novel invention.

---

### Sources)
- [https://www.cnet.com/news/facebooks-valuation-the-cheat-sheet/](https://www.cnet.com/news/facebooks-valuation-the-cheat-sheet/)
- [https://techcrunch.com/2009/05/26/mark-zuckerberg-and-yuri-milner-talk-about-facebooks-new-investment-video/](https://techcrunch.com/2009/05/26/mark-zuckerberg-and-yuri-milner-talk-about-facebooks-new-investment-video/)
- [https://www.nytimes.com/2012/05/16/technology/a-russian-facebook-bet-pays-off-big.html](https://www.nytimes.com/2012/05/16/technology/a-russian-facebook-bet-pays-off-big.html)
- [http://www.dailymail.co.uk/news/article-3784716/Ivanka-Trump-Karlie-Kloss-Wendi-Deng-Murdoch-watch-Open.html](http://www.dailymail.co.uk/news/article-3784716/Ivanka-Trump-Karlie-Kloss-Wendi-Deng-Murdoch-watch-Open.html)
- [https://www.wired.co.uk/article/what-is-the-paradise-papers-leak-facebook-yuri-milner-facebook-twitter-russia](https://www.wired.co.uk/article/what-is-the-paradise-papers-leak-facebook-yuri-milner-facebook-twitter-russia)
- [https://finance.yahoo.com/news/zuckerberg-got-early-business-advice-194957335.html](https://finance.yahoo.com/news/zuckerberg-got-early-business-advice-194957335.html)
- [http://time.com/5011000/paradise-papers-tax-havens-leak/](http://time.com/5011000/paradise-papers-tax-havens-leak/)
- [https://foreignpolicy.com/2017/04/04/russian-spy-met-trump-adviser-carter-page-and-thought-he-was-an-idiot/](https://foreignpolicy.com/2017/04/04/russian-spy-met-trump-adviser-carter-page-and-thought-he-was-an-idiot/)
- [https://techcrunch.com/2009/03/31/confirmed-facebook-loses-cfo-gideon-yu/](https://techcrunch.com/2009/03/31/confirmed-facebook-loses-cfo-gideon-yu/)
- [https://www.ft.com/content/281a8420-a46e-11e1-a701-00144feabdc0](https://www.ft.com/content/281a8420-a46e-11e1-a701-00144feabdc0)
- [http://www.latimes.com/opinion/topoftheticket/la-na-tt-zuckerberg-russians-20170929-story.html](http://www.latimes.com/opinion/topoftheticket/la-na-tt-zuckerberg-russians-20170929-story.html)
- [https://arstechnica.com/tech-policy/2017/10/report-facebook-cut-russia-references-from-report-on-disinformation/](https://arstechnica.com/tech-policy/2017/10/report-facebook-cut-russia-references-from-report-on-disinformation/)
- [http://thehill.com/policy/technology/358102-franken-blasts-facebook-for-accepting-rubles-for-us-election-ads](http://thehill.com/policy/technology/358102-franken-blasts-facebook-for-accepting-rubles-for-us-election-ads)
- [https://mashable.com/2017/10/10/facebook-responds-embeds-trump-campaign-60-minutes/#tDxG85vXHGqr](https://mashable.com/2017/10/10/facebook-responds-embeds-trump-campaign-60-minutes/#tDxG85vXHGqr)
- [https://www.bbc.com/news/av/magazine-40852227/the-digital-guru-who-helped-donald-trump-to-the-presidency](https://www.bbc.com/news/av/magazine-40852227/the-digital-guru-who-helped-donald-trump-to-the-presidency)
- [https://www.theguardian.com/us-news/2017/aug/04/michael-flynn-cambridge-analytica-disclosure](https://www.theguardian.com/us-news/2017/aug/04/michael-flynn-cambridge-analytica-disclosure)
- [http://www.techheadlines.us/facebook-says-it-found-an-insignificant-overlap-between-russia-ads-and-president-trumps-campaign/](http://www.techheadlines.us/facebook-says-it-found-an-insignificant-overlap-between-russia-ads-and-president-trumps-campaign/)
- [https://www.cnbc.com/2018/01/25/facebook-tells-senate-its-software-recommended-russian-propaganda.html](https://www.cnbc.com/2018/01/25/facebook-tells-senate-its-software-recommended-russian-propaganda.html)
- [https://www.buzzfeednews.com/article/kevincollier/a-former-manager-at-the-russian-troll-factory-is-now-living](https://www.buzzfeednews.com/article/kevincollier/a-former-manager-at-the-russian-troll-factory-is-now-living)
- [https://twitter.com/JuliaDavisNews/status/967559297516425217/photo/1](https://twitter.com/JuliaDavisNews/status/967559297516425217/photo/1)
- [https://www.newsweek.com/jared-kushner-ivanka-trump-white-house-forms-omissions-cadre-millions-679231](https://www.newsweek.com/jared-kushner-ivanka-trump-white-house-forms-omissions-cadre-millions-679231)
- [https://www.theverge.com/2016/11/10/13594558/mark-zuckerberg-election-fake-news-trump](https://www.theverge.com/2016/11/10/13594558/mark-zuckerberg-election-fake-news-trump)
- [https://www.theguardian.com/media/2013/jun/13/rupert-murdoch-divorce-wendi-deng](https://www.theguardian.com/media/2013/jun/13/rupert-murdoch-divorce-wendi-deng)
- [http://www.zimbio.com/photos/Yuri+Milner/2016+Time+100+Gala+Time+Most+Influential+People/quud6cf3as-](http://www.zimbio.com/photos/Yuri+Milner/2016+Time+100+Gala+Time+Most+Influential+People/quud6cf3as-)
- [https://www.telegraph.co.uk/fashion/events/time-100-gala-best-dressed-celebrities/model-karlie-kloss-with-wife-of-russian-entrepreneur-yuri-milner/](https://www.telegraph.co.uk/fashion/events/time-100-gala-best-dressed-celebrities/model-karlie-kloss-with-wife-of-russian-entrepreneur-yuri-milner/)
- [https://www.vogue.com/article/businesswoman-wendi-murdoch-career-profile](https://www.vogue.com/article/businesswoman-wendi-murdoch-career-profile)
- [http://www.dailymail.co.uk/news/article-3514484/Wendi-Deng-takes-break-St-Barts-billionaire-friend-Roman-Abramovich-s-yacht-three-weeks-ex-husband-Rupert-Murdoch-tied-knot-Jerry-Hall.html](http://www.dailymail.co.uk/news/article-3514484/Wendi-Deng-takes-break-St-Barts-billionaire-friend-Roman-Abramovich-s-yacht-three-weeks-ex-husband-Rupert-Murdoch-tied-knot-Jerry-Hall.html)
- [http://nymag.com/daily/intelligencer/2008/05/highprofile_bris_on_sunday_you.html](http://nymag.com/daily/intelligencer/2008/05/highprofile_bris_on_sunday_you.html)
- [https://therealdeal.com/2013/11/12/the-donald-sapir-execs-mull-bringing-trump-soho-to-moscow/](https://therealdeal.com/2013/11/12/the-donald-sapir-execs-mull-bringing-trump-soho-to-moscow/)
- [https://www.theguardian.com/business/2003/jul/06/russia.football](https://www.theguardian.com/business/2003/jul/06/russia.football)
- [https://www.independent.co.uk/news/business/analysis-and-features/whats-a-nice-russian-oligarch-like-you-doing-in-a-steelworks-like-this-65258.html](https://www.independent.co.uk/news/business/analysis-and-features/whats-a-nice-russian-oligarch-like-you-doing-in-a-steelworks-like-this-65258.html)