+++
title = "Reading List"
description = "In no particular order, all available freely online."
+++

- Matt Mochary's 
  [Founder to CEO](https://docs.google.com/document/d/1ZJZbv4J6FZ8Dnb0JuMhJxTnwl-dwqx5xl0s65DE3wO8/preview#heading=h.pdmqf3646hgt),
  a guide on how to build a a company from the ground up. It's a great read for seeing just what you don't know about building a company.
- W Ben Hunt's piece about [how coyotes are too clever](http://www.epsilontheory.com/too-clever-by-half/), and what that means for us. This led me to another article by Hunt, about [sheep logic](http://www.epsilontheory.com/sheep-logic/).
- C. Titus Brown's post asking: for an open-source project, [how open is too open](http://ivory.idyll.org/blog/2018-how-open-is-too-open.html)?
- Neal Stephenson's "[In the Beginning was the Command Line](http://cristal.inria.fr/~weis/info/commandline.html)", which takes a long time to paraphrase Alan Kay, who said: _"Keep simple things simple, and make complex things possible."_
- Karl Fogel's [Producing Open Source Software](https://producingoss.com/): probably the best book about making a collaborative project work. The title and language are almost unfortunate, it's message is relevant for every market.

{% admonition() %}
Like what I read? You might like my [blog](https://emsenn.gitlab.io/blog/).
{% end %}